<?php
$conexion = new mysqli("localhost", "root", "", "liga");
if ($conexion->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
}else{
  $resultado = $conexion->query("SELECT * FROM partido ");
}
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <table>
      <tr>
        <td style="color:white;text-align:center;background-color:#2E9AFE">ID_partido</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Local</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Visitante</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Resultado</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Fecha</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Arbitro</td>
      </tr>
      <?php
        foreach ($resultado as $partido) {
          echo "<tr>";
          echo "<td style=text-align:center;>".$partido['id_partido']."</td>";
          echo "<td style=text-align:center;>"."<a href='equipo.php?equipos=".$partido['local']."'>".$partido['local']."</td>";
          echo "<td style=text-align:center;>"."<a href='equipo.php?equipos=".$partido['visitante']."'>".$partido['visitante']."</td>";
          echo "<td style=text-align:center;>".$partido['resultado']."</td>";
          echo "<td style=text-align:center;>".$partido['fecha']."</td>";
          echo "<td style=text-align:center;>".$partido['arbitro']."</td>";
          echo "</tr>";
        }
      ?>
    </table>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>
