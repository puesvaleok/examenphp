<?php
$conexion = new mysqli("localhost", "root", "", "liga");
if ($conexion->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
}else{
    $id2=$_GET["idEquipo"];
  $resultado = $conexion->query("SELECT * FROM jugador WHERE equipo=".$id2);
}
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <table>
      <tr>
        <td style="color:white;text-align:center;background-color:#2E9AFE">id_jugador</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Nombre</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Apellido</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Posición</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">ID_capitan</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Fecha alta</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Salario</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Equipo</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Altura</td>
      </tr>
      <?php
        foreach ($resultado as $jugador) {
          echo "<tr>";
          echo "<td style=text-align:center;>".$jugador['id_jugador']."</td>";
          echo "<td style=text-align:center;>".$jugador['nombre']."</td>";
          echo "<td style=text-align:center;>".$jugador['apellido']."</td>";
          if ($jugador['posicion']=="Base") {
            echo "<td style=text-align:center;font-weight:bold;>".$jugador['posicion']."</td>";
          }
          else {
          echo "<td style=text-align:center;>".$jugador['posicion']."</td>";
          }
          echo "<td style=text-align:center;>".$jugador['id_capitan']."</td>";
          echo "<td style=text-align:center;>".$jugador['fecha_alta']."</td>";
          echo "<td style=text-align:center;>".$jugador['salario']."</td>";
          echo "<td style=text-align:center;>".$jugador['equipo']."</td>";
          echo "<td style=text-align:center;>".$jugador['altura']."</td>";
          echo "</tr>";
        }
      ?>
    </table>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js">



</script>
  </body>
</html>
