<?php
$conexion = new mysqli("localhost", "root", "", "liga");
if ($conexion->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
}else{
  $id=$_GET["equipos"];
  $resultado = $conexion->query("SELECT * FROM equipo WHERE id_equipo=".$id );
}
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <table>
      <tr>
        <td style="color:white;text-align:center;background-color:#2E9AFE">ID_equipo</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Nombre</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Ciudad</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Web</td>
        <td style="color:white;text-align:center;background-color:#2E9AFE">Puntos</td>
      </tr>
      <?php
        foreach ($resultado as $equipo) {
          echo "<tr>";
          echo "<td style=text-align:center;>"."<a href='jugadores.php?idEquipo=".$equipo['id_equipo']."'>".$equipo['id_equipo']."</a></td>";
          echo "<td style=text-align:center;>".$equipo['nombre']."</td>";
          echo "<td style=text-align:center;>".$equipo['ciudad']."</td>";
          echo "<td style=text-align:center;>".$equipo['web']."</td>";
          echo "<td style=text-align:center;>".$equipo['puntos']."</td>";
          echo "</tr>";
        }
      ?>
    </table>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>
